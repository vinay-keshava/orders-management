package com.training.order.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.order.entity.Orders;
import com.training.order.repository.OrderRepository;
import com.training.order.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService{
	
	@Autowired
	OrderRepository orderRepository;
	
	public Orders saveOrders( Orders  orders) {
		List<Double> productsPrice = orders.getProducts().stream()
				.map(p -> p.getPrice())
				.collect(Collectors.toList());
		Double totalPrice = productsPrice.stream()
				.mapToDouble(Double::doubleValue).sum();
		orders.setOrderTotal(totalPrice);
		return orderRepository.save(orders);
	}
}
