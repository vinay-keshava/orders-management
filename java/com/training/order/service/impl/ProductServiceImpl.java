package com.training.order.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.order.entity.Products;
import com.training.order.repository.ProductRepository;
import com.training.order.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	ProductRepository productRepository;

	public Products newproduct(Products products) {
		return productRepository.save(products);
	}
	
	public List<Products> getAllProducts(){
		return productRepository.findAll();
	}

}
