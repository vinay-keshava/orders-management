package com.training.order.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class Customer {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private long customerId;
	
	private String firstName;
	private String lastName;
	private int age;
	private String address;
	private int pinCode;
	private String phoneNumber;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<Orders> orders;
	

	
}
