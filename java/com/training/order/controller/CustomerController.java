package com.training.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.order.entity.Customer;
import com.training.order.service.impl.CustomerServiceImpl;

@RestController
@RequestMapping("/customers")
public class CustomerController {

	@Autowired
	CustomerServiceImpl customerServiceImpl;
	
	@PostMapping("/")
	public Customer newCustomer(@RequestBody Customer customer) {
		return customerServiceImpl.saveCustomer(customer);
	}
}
