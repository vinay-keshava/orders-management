package com.training.order.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.order.entity.Products;
import com.training.order.service.impl.ProductServiceImpl;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	ProductServiceImpl productServiceImpl;

	@PostMapping("/")
	public Products newProduct(@RequestBody Products product) {
		return productServiceImpl.newproduct(product);
	}
	
	@GetMapping("/")
	public List<Products> getAllProducts(){
		return productServiceImpl.getAllProducts();
				
	}
}
